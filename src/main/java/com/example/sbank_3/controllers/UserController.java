package com.example.sbank_3.controllers;


import com.example.sbank_3.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import com.example.sbank_3.repositories.UserRepository;

@Controller
@RequestMapping()
public class UserController {
    @Autowired
    private UserRepository userRepository;


    @PostMapping(path = "/add")
    public @ResponseBody
    String addNewUser(@RequestParam String name, @RequestParam String surname, @RequestParam String mobile, @RequestParam String country, @RequestParam String address) {
        User user1 = new User(name, surname, mobile, country, address);

        user1.setName(name);
        user1.setSurname(surname);
        user1.setMobile(mobile);
        user1.setCountry(country);
        user1.setAddress(address);
        userRepository.save(user1);
        return "Saved";
    }

    /*@GetMapping(path = "/all")
    public @ResponseBody
    Iterable<User> getAllUsers() {
        return userRepository.findAll();
    }*/

    @GetMapping({"/showusers"})
    public String showUsers(Model model) {
        Iterable<User> users = userRepository.findAll();
        model.addAttribute("clients", users);
        return "views/showusers";
    }

}
