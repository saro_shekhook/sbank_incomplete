package com.example.sbank_3.repositories;


import com.example.sbank_3.models.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Integer> {
}