package com.example.sbank_3;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SBank3Application {

    public static void main(String[] args) {
        SpringApplication.run(SBank3Application.class, args);
    }

}
